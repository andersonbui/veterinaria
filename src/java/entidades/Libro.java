/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author debian
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Libro.findAll", query = "SELECT l FROM Libro l")
    , @NamedQuery(name = "Libro.findByPubIdentificador", query = "SELECT l FROM Libro l WHERE l.pubIdentificador = :pubIdentificador")
    , @NamedQuery(name = "Libro.findByLibTituloLibro", query = "SELECT l FROM Libro l WHERE l.libTituloLibro = :libTituloLibro")
    , @NamedQuery(name = "Libro.findByLibIsbn", query = "SELECT l FROM Libro l WHERE l.libIsbn = :libIsbn")
    , @NamedQuery(name = "Libro.findByPais", query = "SELECT l FROM Libro l WHERE l.pais = :pais")
    , @NamedQuery(name = "Libro.findByCiudad", query = "SELECT l FROM Libro l WHERE l.ciudad = :ciudad")
    , @NamedQuery(name = "Libro.findByEditorial", query = "SELECT l FROM Libro l WHERE l.editorial = :editorial")})
public class Libro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "pub_identificador", nullable = false)
    private Integer pubIdentificador;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "lib_titulo_libro", nullable = false, length = 200)
    private String libTituloLibro;
    @Size(max = 30)
    @Column(name = "lib_isbn", length = 30)
    private String libIsbn;
    @Size(max = 100)
    @Column(length = 100)
    private String pais;
    @Size(max = 100)
    @Column(length = 100)
    private String ciudad;
    @Size(max = 25)
    @Column(length = 25)
    private String editorial;
    @JoinColumn(name = "ciudad_id", referencedColumnName = "ciud_id")
    @ManyToOne
    private Ciudad ciudadId;
    @JoinColumn(name = "pub_identificador", referencedColumnName = "pub_identificador", nullable = false, insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Publicacion publicacion;

    public Libro() {
    }

    public Libro(Integer pubIdentificador) {
        this.pubIdentificador = pubIdentificador;
    }

    public Libro(Integer pubIdentificador, String libTituloLibro) {
        this.pubIdentificador = pubIdentificador;
        this.libTituloLibro = libTituloLibro;
    }

    public Integer getPubIdentificador() {
        return pubIdentificador;
    }

    public void setPubIdentificador(Integer pubIdentificador) {
        this.pubIdentificador = pubIdentificador;
    }

    public String getLibTituloLibro() {
        return libTituloLibro;
    }

    public void setLibTituloLibro(String libTituloLibro) {
        this.libTituloLibro = libTituloLibro;
    }

    public String getLibIsbn() {
        return libIsbn;
    }

    public void setLibIsbn(String libIsbn) {
        this.libIsbn = libIsbn;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public Ciudad getCiudadId() {
        return ciudadId;
    }

    public void setCiudadId(Ciudad ciudadId) {
        this.ciudadId = ciudadId;
    }

    public Publicacion getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(Publicacion publicacion) {
        this.publicacion = publicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pubIdentificador != null ? pubIdentificador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Libro)) {
            return false;
        }
        Libro other = (Libro) object;
        if ((this.pubIdentificador == null && other.pubIdentificador != null) || (this.pubIdentificador != null && !this.pubIdentificador.equals(other.pubIdentificador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Libro[ pubIdentificador=" + pubIdentificador + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author debian
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Publicacion.findAll", query = "SELECT p FROM Publicacion p")
    , @NamedQuery(name = "Publicacion.findByPubIdentificador", query = "SELECT p FROM Publicacion p WHERE p.pubIdentificador = :pubIdentificador")
    , @NamedQuery(name = "Publicacion.findByPubHash", query = "SELECT p FROM Publicacion p WHERE p.pubHash = :pubHash")
    , @NamedQuery(name = "Publicacion.findByPubDiropkm", query = "SELECT p FROM Publicacion p WHERE p.pubDiropkm = :pubDiropkm")
    , @NamedQuery(name = "Publicacion.findByPubFechaVisado", query = "SELECT p FROM Publicacion p WHERE p.pubFechaVisado = :pubFechaVisado")
    , @NamedQuery(name = "Publicacion.findByPubFechaRegistro", query = "SELECT p FROM Publicacion p WHERE p.pubFechaRegistro = :pubFechaRegistro")
    , @NamedQuery(name = "Publicacion.findByPubEstado", query = "SELECT p FROM Publicacion p WHERE p.pubEstado = :pubEstado")
    , @NamedQuery(name = "Publicacion.findByPubAutoresSecundarios", query = "SELECT p FROM Publicacion p WHERE p.pubAutoresSecundarios = :pubAutoresSecundarios")
    , @NamedQuery(name = "Publicacion.findByPubTipoPublicacion", query = "SELECT p FROM Publicacion p WHERE p.pubTipoPublicacion = :pubTipoPublicacion")
    , @NamedQuery(name = "Publicacion.findByPubFechaPublicacion", query = "SELECT p FROM Publicacion p WHERE p.pubFechaPublicacion = :pubFechaPublicacion")
    , @NamedQuery(name = "Publicacion.findByPubNumActa", query = "SELECT p FROM Publicacion p WHERE p.pubNumActa = :pubNumActa")
    , @NamedQuery(name = "Publicacion.findByPubVisado", query = "SELECT p FROM Publicacion p WHERE p.pubVisado = :pubVisado")
    , @NamedQuery(name = "Publicacion.findByTipoDocumento", query = "SELECT p FROM Publicacion p WHERE p.tipoDocumento = :tipoDocumento")
    , @NamedQuery(name = "Publicacion.findByPubCreditos", query = "SELECT p FROM Publicacion p WHERE p.pubCreditos = :pubCreditos")})
public class Publicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pub_identificador", nullable = false)
    private Integer pubIdentificador;
    @Size(max = 40)
    @Column(name = "pub_hash", length = 40)
    private String pubHash;
    @Size(max = 30)
    @Column(name = "pub_diropkm", length = 30)
    private String pubDiropkm;
    @Column(name = "pub_fecha_visado")
    @Temporal(TemporalType.DATE)
    private Date pubFechaVisado;
    @Column(name = "pub_fecha_registro")
    @Temporal(TemporalType.DATE)
    private Date pubFechaRegistro;
    @Size(max = 15)
    @Column(name = "pub_estado", length = 15)
    private String pubEstado;
    @Size(max = 300)
    @Column(name = "pub_autores_secundarios", length = 300)
    private String pubAutoresSecundarios;
    @Size(max = 22)
    @Column(name = "pub_tipo_publicacion", length = 22)
    private String pubTipoPublicacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pub_fecha_publicacion", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date pubFechaPublicacion;
    @Column(name = "pub_num_acta")
    private Integer pubNumActa;
    @Size(max = 20)
    @Column(name = "pub_visado", length = 20)
    private String pubVisado;
    @Column(name = "tipo_documento")
    private Integer tipoDocumento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pub_creditos", nullable = false)
    private int pubCreditos;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "publicacion")
    private Libro libro;
    @JoinColumn(name = "pub_est_identificador", referencedColumnName = "est_identificador")
    @ManyToOne
    private Estudiante pubEstIdentificador;
    @JoinColumn(name = "id_tipo_documento", referencedColumnName = "identificador")
    @ManyToOne
    private TipoDocumento idTipoDocumento;

    public Publicacion() {
    }

    public Publicacion(Integer pubIdentificador) {
        this.pubIdentificador = pubIdentificador;
    }

    public Publicacion(Integer pubIdentificador, Date pubFechaPublicacion, int pubCreditos) {
        this.pubIdentificador = pubIdentificador;
        this.pubFechaPublicacion = pubFechaPublicacion;
        this.pubCreditos = pubCreditos;
    }

    public Integer getPubIdentificador() {
        return pubIdentificador;
    }

    public void setPubIdentificador(Integer pubIdentificador) {
        this.pubIdentificador = pubIdentificador;
    }

    public String getPubHash() {
        return pubHash;
    }

    public void setPubHash(String pubHash) {
        this.pubHash = pubHash;
    }

    public String getPubDiropkm() {
        return pubDiropkm;
    }

    public void setPubDiropkm(String pubDiropkm) {
        this.pubDiropkm = pubDiropkm;
    }

    public Date getPubFechaVisado() {
        return pubFechaVisado;
    }

    public void setPubFechaVisado(Date pubFechaVisado) {
        this.pubFechaVisado = pubFechaVisado;
    }

    public Date getPubFechaRegistro() {
        return pubFechaRegistro;
    }

    public void setPubFechaRegistro(Date pubFechaRegistro) {
        this.pubFechaRegistro = pubFechaRegistro;
    }

    public String getPubEstado() {
        return pubEstado;
    }

    public void setPubEstado(String pubEstado) {
        this.pubEstado = pubEstado;
    }

    public String getPubAutoresSecundarios() {
        return pubAutoresSecundarios;
    }

    public void setPubAutoresSecundarios(String pubAutoresSecundarios) {
        this.pubAutoresSecundarios = pubAutoresSecundarios;
    }

    public String getPubTipoPublicacion() {
        return pubTipoPublicacion;
    }

    public void setPubTipoPublicacion(String pubTipoPublicacion) {
        this.pubTipoPublicacion = pubTipoPublicacion;
    }

    public Date getPubFechaPublicacion() {
        return pubFechaPublicacion;
    }

    public void setPubFechaPublicacion(Date pubFechaPublicacion) {
        this.pubFechaPublicacion = pubFechaPublicacion;
    }

    public Integer getPubNumActa() {
        return pubNumActa;
    }

    public void setPubNumActa(Integer pubNumActa) {
        this.pubNumActa = pubNumActa;
    }

    public String getPubVisado() {
        return pubVisado;
    }

    public void setPubVisado(String pubVisado) {
        this.pubVisado = pubVisado;
    }

    public Integer getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(Integer tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public int getPubCreditos() {
        return pubCreditos;
    }

    public void setPubCreditos(int pubCreditos) {
        this.pubCreditos = pubCreditos;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public Estudiante getPubEstIdentificador() {
        return pubEstIdentificador;
    }

    public void setPubEstIdentificador(Estudiante pubEstIdentificador) {
        this.pubEstIdentificador = pubEstIdentificador;
    }

    public TipoDocumento getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(TipoDocumento idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pubIdentificador != null ? pubIdentificador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Publicacion)) {
            return false;
        }
        Publicacion other = (Publicacion) object;
        if ((this.pubIdentificador == null && other.pubIdentificador != null) || (this.pubIdentificador != null && !this.pubIdentificador.equals(other.pubIdentificador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Publicacion[ pubIdentificador=" + pubIdentificador + " ]";
    }
    
}
